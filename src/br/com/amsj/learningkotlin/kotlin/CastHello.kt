package br.com.amsj.learningkotlin.kotlin

import java.util.*

fun main(args: Array<String>) {

    var objectMaroto : Any

    val test = Random().nextInt()
    println("Test value is $test")

    if(test > 5){
        objectMaroto = "Valor Maior que 5"
    }else{
        objectMaroto = 10;
    }

    if(objectMaroto is String){
        var test3 = objectMaroto
    }
    else if(objectMaroto is Int){
        var test4 = objectMaroto

        println("${test4::class.qualifiedName}")
    }
    println("${objectMaroto::class.qualifiedName}")
    println("objectMaroto: $objectMaroto")
}