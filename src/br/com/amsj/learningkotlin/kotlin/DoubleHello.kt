package br.com.amsj.learningkotlin.kotlin

import java.math.BigDecimal

fun main(args: Array<String>) {

    val myByte : Byte = 1
    val myShort : Short = 8
    val myChar : Char = 'a'
    val myInt : Int = 20
    val myLong : Long = 20L
    val myFloat : Float = 13.6F
    val myDouble = 22.8
    val myBoolean : Boolean = true

    println("myByte: ${myByte}")
    println("myShort: ${myShort}")
    println("myChar: ${myChar}")
    println("myInt: ${myInt}")
    println("myLong: ${myLong}")
    println("myFloat: ${myFloat}")
    println("myDouble: ${myDouble}")
    println("myBoolean: ${myBoolean}")

    val db1 = BigDecimal(10);

    var db2 = db1.add(BigDecimal(20))

    println("--> $db2")



}