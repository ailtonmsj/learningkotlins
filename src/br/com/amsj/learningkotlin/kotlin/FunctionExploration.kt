package br.com.amsj.learningkotlin.kotlin

fun addTwoNumber(one: Double, two: Double) = one + two

fun someMaths(one: Double, two: Double = 30.0){
    println("One + two is ${one + two}")
    println("One - two is ${one - two}")
}


fun main(args: Array<String>) {

    val one = 10.0
    val two = 20.0

    println("Double sum ${addTwoNumber(one, two)}")

    someMaths(10.0, 20.0)
    someMaths(two = 10.0, one = 20.0)
    someMaths(one = 10.0)
    someMaths(one = 10.5, two = 10.0)

}
