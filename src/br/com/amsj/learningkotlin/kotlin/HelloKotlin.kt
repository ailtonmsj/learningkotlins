package br.com.amsj.learningkotlin.kotlin

fun main(args: Array<String>) {

    var name: String = "Bruce"
    val surname: String = "Wayne"

    name = "Robert"

    println("Hello $name ${surname.toUpperCase()}")
    println("The \$name variable has ${name.length} characters");
    println("Your product cost about $10")

    val story = """A long time ago
        @#there is a place call biruleibe
        @#a crazy place to be
        @#with crazy people living there"""

    println(story.trimMargin("@#"));


}
