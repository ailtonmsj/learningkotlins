package br.com.amsj.learningkotlin.kotlin

data class Book (val id: Long, val nome: String, val isbn: String="") {

    constructor(id: Long, nome: String) : this(id, nome, ""){
        println("secondary constructor")
    }

    init {
        println("init block")
    }

    var completo = false

    set(value){
        if(!isbn.isNullOrBlank()) {
            field = value
        }else{
            field = false
        }
    }

    fun uppperCaseNome() =
            this.nome.toUpperCase()

    /*
    override fun toString(): String {
        return "$id - $nome"
    }
    */

    companion object {
        fun getInstance() =
                Book(3, "Harry Potter")

        val identificadorClasse = "Book"
    }

}

fun main(args: Array<String>) {

    val livro1 = Book(1, "Lord of the Rings")
    val livro2 = Book(2, "The Hobbit", "134567987-X")

    livro1.completo = true
    livro2.completo = true

    println("${livro1.id} is ${livro1.uppperCaseNome()} and isbn is ${livro1.isbn}. Are the data complete? ${livro1.completo}")
    println("${livro2.id} is ${livro2.uppperCaseNome()} and isbn is ${livro2.isbn}. Are the data complete? ${livro2.completo}")

    println("$livro1")
    println("$livro2")

    val livro3 = Book.getInstance()

    println(livro3)
    println(Book.identificadorClasse)

    val livro4 = livro3.copy(nome="Fire and Ice Chronic")

    println(livro4)

}



